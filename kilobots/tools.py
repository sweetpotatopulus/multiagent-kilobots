"""
Tools required for the agent behaviour.

Autor: Raul Maldonado
"""

from collections import defaultdict


def compare_sensor(sensor, condition):
    """Compare sensors versus a set of conditions."""
    comparison = [x == y if y != '_' else True for x,
                  y in zip(sensor, condition)]
    return all(comparison)


def get_gradient_fsm(neighs):
    """Compute next gradient."""
    neig_pos = [x.whichgradient for x in neighs if x.whichgradient]
    if neig_pos:
        return min(neig_pos)+1
    return False


def get_edge_fsm(neighs, gradient):
    """Compute edges."""
    neig_pos = [x.whichgradient for x in neighs]
    # if not validate_neighbors(neighs):
    #     return False
    if all(neig_pos):
        if gradient >= max(neig_pos):
            return True
    return False


def get_new_coors(coors, next_move):
    """Get next move."""
    moves_dict = {'downleft': (-1, -1),
                  'down': (0, -1),
                  'downright': (1, -1),
                  'right': (1, 0),
                  'upright': (1, 1),
                  'up': (0, 1),
                  'upleft': (-1, 1),
                  'left': (-1, 0),
                  'wait': (0, 0)}

    nmove = moves_dict[next_move]
    return coors[0]+nmove[0], coors[1]+nmove[1]


def get_neighborloc(n_coor, my_coor):
    """Get the coordinates of the neighbors."""
    if n_coor[0] == my_coor[0]:
        if n_coor[1] == my_coor[1]+1:
            return 'up'
        return 'down'
    if n_coor[1] == my_coor[1]:
        if n_coor[0] == my_coor[0]+1:
            return 'right'
        return 'left'
    if n_coor[0] == my_coor[0]+1:
        if n_coor[1] == my_coor[1]+1:
            return 'upright'
        return 'downright'
    if n_coor[0] == my_coor[0]-1:
        if n_coor[1] == my_coor[1]+1:
            return 'upleft'
    return 'downleft'


def get_gradient(neighs, grad):
    """Compute next gradient."""
    if grad:
        return grad
    neig_pos = [x for x in neighs if x]
    if neig_pos:
        return min(neig_pos)+1
    return False


def validate_neighbors(neighs):
    """Validate moving neighbors."""
    neig_state = set(x.state for x in neighs)
    if neig_state.intersection(['moving', 'edge']):
        return False
    return True


def get_edge(neig_pos, gradient):
    """Compute edges."""
    if not gradient:
        return False
    if all(neig_pos):
        if gradient >= max(neig_pos):
            return True
    return False


def select_next_move(nposs, nstate):
    """Select next move."""
    moves = ['downleft', 'down', 'downright', 'right', 'upright', 'up',
             'upleft', 'left', 'down']
    next_move = False
    for move in moves:
        if move in nposs:
            next_move = move
            mindex = nposs.index(move)
        elif next_move:
            if nstate[mindex] in ['moving']:
                return 'wait'
            # if (move == "down") and (nstate[mindex] == "idle") and (next_move == 'left'):
            #     return "downleft"
            if (move == "down") and ("up" in nposs) and ("downleft" not in nposs):
                return "downleft"
            if (move == "downright") and ("right" in nposs):
                continue
            if (move == "down") and ("right" in nposs):
                continue
            else:
                return move
    return "lost"


def evaluate_gradient_reference(rel_pos, nposs, nstate, ngradient):
    """Calculate relative position."""
    val_ref = {'left': 0,
               'upleft': -1,
               'downleft': +1,
               'down': 0
               }
    if rel_pos in nposs:
        dindex = nposs.index(rel_pos)
        if (nstate[dindex] in ['seed', 'positioned']):
            return ngradient[dindex] + val_ref[rel_pos]
    return False


def evaluate_gradient_reference_wrapp(nposs, nstate, ngradient):
    rel_pos = ['left', 'upleft', 'downleft']
    rels = [evaluate_gradient_reference(
        x, nposs, nstate, ngradient) for x in rel_pos]
    if any(rels):
        return max(rels)


# GRADIENTS_DICT = defaultdict(lambda: (0, -1))
# GRADIENTS_HELP = {0: (10, 1),
#                   1: (2, 1),
#                   2: (40, 2),
#                   3: (11, 3),
#                   4: (12, 2),
#                   5: (11, 1),
#                   6: (12, 2),
#                   7: (13, 3)}
ROBOT_SWORD = {0: (0+1, 25+1),
               1: (0+1, 29+1),
               2: (0+1, 30+1),
               3: (2+1, 31+1),
               4: (1+1, 31+1),
               5: (0+1, 30+1),
               6: (0+1, 27+1),
               7: (22+1, 25+1),
               8: (22+1, 24+1),
               9: (22+1, 24+1),
               10: (22+1, 24+1),
               11: (22+1, 24+1),
               12: (23+1, 26+1),
               13: (23+1, 25+1),
               14: (23+1, 26+1),
               15: (25+1, 27+1),
               16: (27+1, 28+1),
               17: (28+1, 29+1),
               18: (29+1, 30+1),
               19: (29+1, 31+1),
               20: (31+1, 33+1),
               21: (32+1, 33+1),
               }

TRIANGLE = {0: (1, 2+2),
             1: (1, 3+2),
             2: (1, 4+2),
             3: (1, 5+2),
             4: (1, 6+2),
             5: (1, 7+2),
             6: (1, 8+2),
             7: (1, 7+2),
             8: (1, 6+2),
             9: (1, 5+2),
             10: (1, 4+2),
             11: (1, 3+2),
             12: (1, 2+2),
             13: (1, 3+2),
             14: (1, 4+2),
             15: (1, 5+2),
             16: (1, 6+2),
             17: (1, 7+2),
             18: (1, 8+2),
             19: (1, 7+2),
             20: (1, 6+2),
             21: (1, 5+2),
             22: (1, 4+2),
             23: (1, 3+2),
             }

TAJ_MAJAL = {0: (1, 2),
             1: (1, 16),
             2: (1, 30),
             3: (1, 31),
             4: (1, 12),
             5: (1, 2),
             6: (1, 14),
             7: (1, 14),
             8: (1, 19),
             9: (1, 22),
             10: (1, 24),
             11: (1, 22),
             12: (1, 19),
             13: (1, 14),
             14: (1, 14),
             15: (1, 2),
             16: (1, 16),
             17: (1, 30),
             18: (1, 31),
             19: (1, 12),
             20: (1, 2),
             }

# for key, val in ROBOT_SWORD.items():
#     GRADIENTS_DICT[key] = val
