from mesa import Agent

from .tools import *

STABLESTATES = ['seed', 'lost', 'positioned']


class Cell(Agent):
    """Represents a single ALIVE or DEAD cell in the simulation."""

    DEAD = "idle"
    ALIVE = "seed"

    def __init__(self, pos, kilobot_id, model, gradients_dict,
                 init_state="idle", seed=None):
        """
        Create a cell, in the given state, at the given x, y position.
        """
        super().__init__(pos, model)
        self.gradients_dict = gradients_dict
        self.kilobot_id = kilobot_id
        self.gradient = self._nextgradient = False
        self.x, self.y = pos
        self.state = init_state
        self._nextcoors = (self.x, self.y)

    @property
    def isAlive(self):
        return self.state

    @property
    def whichgradient(self):
        return self.gradient

    @property
    def whereami(self):
        return self.x, self.y

    @property
    def neighbors(self):
        return self.model.grid.neighbor_iter((self.x, self.y), True)

    def sensor_neighbors(self):
        nposs = [get_neighborloc((x.x, x.y),
                                 (self.x, self.y)) for x in self.neighbors]
        nstate = [x.state for x in self.neighbors]
        ngradient = [x.gradient for x in self.neighbors]
        return nposs, nstate, ngradient

    def get_is_on_square(self, nposs, nstate, ngradient):
        """Test if on square."""
        rel_d = evaluate_gradient_reference('down', nposs, nstate, ngradient)
        if (rel_d) and (rel_d < self.gradients_dict[self.x-28][1]):
            self._nextgradient = rel_d + 1
            return True
        rel_left = evaluate_gradient_reference_wrapp(nposs, nstate, ngradient)
        if (rel_left) and (rel_left == self.gradients_dict[self.x-28][0]):
            self._nextgradient = rel_left
            return True

    def edge_following(self):
        """Do edge following."""
        nposs = [get_neighborloc((x.x, x.y),
                                 (self.x, self.y)) for x in self.neighbors]
        nstate = [x.state for x in self.neighbors]
        next_move = select_next_move(nposs, nstate)
        if next_move == 'lost':
            self.state = 'lost'
            return
        self._nextcoors = get_new_coors((self.x, self.y), next_move)

    def step(self):
        """Compute an iteration step."""
        self._nextgradient = self.gradient
        self._nextcoors = (self.x, self.y)
        # Sensor Layer
        # # Get the gradient, positon, and state of neighbors
        n_poss, n_state, n_grad = self.sensor_neighbors()
        # # sense is the neighbors around are moving (True means u good 2 go)
        n_moving = validate_neighbors(self.neighbors)
        # # Detect if the bot is in appropriate part of the image
        is_on_square = self.get_is_on_square(n_poss, n_state, n_grad)
        # Sensor signal ("Current_state", "is on shape?", "is neigbor moving?",
        # "kilobot's gradient", "neigbors gradient")
        sensors = (self.state, is_on_square, n_moving, self.gradient, n_grad)

        # Positioning Layer
        if compare_sensor(sensors, ('moving', True, '_', '_', '_')):
            self.state = 'positioned'
        # Edge following Layer
        elif compare_sensor(sensors, ('moving', '_', '_', '_', '_')):
            self.edge_following()
        # Begin movement Layer
        elif compare_sensor(sensors, ('edge', '_', True, '_', '_')):
            # # Start moving with a certain probability
            if self.random.random() < .1:
                self.state = 'moving'
                # self.edge_following()
        # Compare and compute gradient Layer
        elif compare_sensor(sensors, ('idle', '_', True, '_', '_')):
            self._nextgradient = get_gradient(sensors[-1], sensors[-2])
            if get_edge(sensors[-1], sensors[-2]):
                self.state = 'edge'

    def advance(self):
        """Set the state to the new computed state."""
        self.gradient = self._nextgradient
        self.x, self.y = self._nextcoors
        self.model.grid.move_agent(self, self._nextcoors)
