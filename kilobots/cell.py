from mesa import Agent

from .tools import *

STABLESTATES = ['seed', 'lost', 'positioned']


class Cell(Agent):
    """Represents a single ALIVE or DEAD cell in the simulation."""

    DEAD = "idle"
    ALIVE = "seed"

    def __init__(self, pos, kilobot_id, model, gradients_dict,
                 init_state="idle", seed=None):
        """
        Create a cell, in the given state, at the given x, y position.
        """
        super().__init__(pos, model)
        self.gradients_dict = gradients_dict
        self.kilobot_id = kilobot_id
        self.gradient = self._nextgradient = False
        self.x, self.y = pos
        self.state = self._nextState = init_state
        self._nextcoors = (self.x, self.y)

    @property
    def isAlive(self):
        return self.state

    @property
    def whichgradient(self):
        return self.gradient

    @property
    def whereami(self):
        return self.x, self.y

    @property
    def neighbors(self):
        return self.model.grid.neighbor_iter((self.x, self.y), True)

    def is_on_square(self):
        """Test if on square."""
        nposs = [get_neighborloc((x.x, x.y),
                                 (self.x, self.y)) for x in self.neighbors]
        nstate = [x.state for x in self.neighbors]
        ngradient = [x.gradient for x in self.neighbors]
        rel_d = evaluate_gradient_reference('down', nposs, nstate, ngradient)
        if (rel_d) and (rel_d < self.gradients_dict[self.x-28][1]):
            self.state = 'positioned'
            self._nextgradient = rel_d + 1
            return True
        rel_left = evaluate_gradient_reference_wrapp(nposs, nstate, ngradient)
        if (rel_left) and (rel_left == self.gradients_dict[self.x-28][0]):
            self.state = 'positioned'
            self._nextgradient = rel_left
            return True

    def edge_following(self):
        """Do edge following."""
        nposs = [get_neighborloc((x.x, x.y),
                                 (self.x, self.y)) for x in self.neighbors]
        nstate = [x.state for x in self.neighbors]
        next_move = select_next_move(nposs, nstate)
        if next_move == 'lost':
            self.state = 'lost'
            return
        self._nextcoors = get_new_coors((self.x, self.y), next_move)

    def step(self):
        """Compute an iteration step."""
        self._nextgradient = self.gradient
        self._nextcoors = (self.x, self.y)

        if 'lost' in [x.state for x in self.neighbors]:
            print(self.state, self.gradient, [x.gradient for x in self.neighbors])
        if self.state in STABLESTATES:
            return
        if self.state == 'moving':
            # if self.random.random() < .005:
            #     self.state = 'lost'
            #     self._nextgradient = 0
            if self.is_on_square():
                return
            self.edge_following()
        elif not validate_neighbors(self.neighbors):
            return
        elif self.state == 'edge':
            # Adding noise to the system
            # if self.random.random() < .001:
            #     self.state = 'lost'
            #     self._nextgradient = 1
            if self.random.random() < .9:
                self.edge_following()
                self.state = 'moving'
                self._nextgradient = 1
        # IDLE  behaviour
        elif not self.gradient:
            self._nextgradient = get_gradient_fsm(self.neighbors)
        elif get_edge_fsm(self.neighbors, self.gradient):
            self.state = 'edge'

    def advance(self):
        """Set the state to the new computed state."""
        self.state = self.state
        self.gradient = self._nextgradient
        self.x, self.y = self._nextcoors
        self.model.grid.move_agent(self, self._nextcoors)
