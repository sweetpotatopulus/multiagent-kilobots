from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.UserParam import UserSettableParameter

from .portrayal import portrayCell
from .model import KilobotsShapes

WSIZE = 65
DSIZE = 845
# DSIZE = 195

model_params = {"height": WSIZE,
                "width": WSIZE,
                "nkilobots": UserSettableParameter("slider",
                                                   "Kilobot Population",
                                                   20, 10, 300)
                }

# Make a world that is 50x50, on a 250x250 display.
canvas_element = CanvasGrid(portrayCell, WSIZE, WSIZE, DSIZE, DSIZE)

server = ModularServer(
    KilobotsShapes, [canvas_element], "Kilobots",
    model_params
)
