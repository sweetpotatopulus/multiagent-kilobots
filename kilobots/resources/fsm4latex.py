def step(self):
    """Compute an iteration step."""
    # Robot is in "Positioned" state
    if self.state in STABLESTATES:
        return
    # Robot is "Moving"
    elif self.state == 'moving':
        if self.is_on_square():
            return
        self.edge_following()
    # Robot is "Edge"
    elif self.state == 'edge':
        if self.random.random() < .1:
            self.edge_following()
            self.state = 'moving'
            self._nextgradient = 0
    # Robot is "Idle"
    elif not self.gradient:
        self._nextgradient = get_gradient_fsm(self.neighbors)
        if get_edge_fsm(self.neighbors, self.gradient):
            self.state = 'edge'
