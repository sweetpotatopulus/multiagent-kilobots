def step(self):
    """Compute an iteration step."""
    # Sensing Layer
    # # Get the gradient, positon, and state of neighbors
    n_poss, n_state, n_grad = self.sensor_neighbors()
    # # sense is the neighbors around are moving (True means u good 2 go)
    n_moving = validate_neighbors(self.neighbors)
    # # Detect if the bot is in appropriate part of the image
    is_on_square = self.get_is_on_square(n_poss, n_state, n_grad)
    # Sensor signal ("Current_state", "is on shape?", "is neigbor moving?",
    # "kilobot's gradient", "neigbors gradient")
    sensors = (self.state, is_on_square, n_moving, self.gradient, n_grad)

    # Positioning Layer
    if compare_sensor(sensors, ('moving', True, '_', '_', '_')):
        self.state = 'positioned'
    # Edge following Layer
    elif compare_sensor(sensors, ('moving', '_', '_', '_', '_')):
        self.edge_following()
    # Begin movement Layer
    elif compare_sensor(sensors, ('edge', '_', True, '_', '_')):
        # # Start moving with a certain probability
        if self.random.random() < .1:
            self.state = 'moving'
            # self.edge_following()
    # Compare and compute gradient Layer
    elif compare_sensor(sensors, ('idle', '_', True, '_', '_')):
        self._nextgradient = get_gradient(sensors[-1], sensors[-2])
        if get_edge(sensors[-1], sensors[-2]):
            self.state = 'edge'
