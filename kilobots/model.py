"""
Implementation the model for the kilobots.

Date: 18/05/2020
"""
from collections import defaultdict
from mesa import Model
from mesa.time import SimultaneousActivation
from mesa.space import Grid
import numpy as np

from .cell import Cell as Fsm
from .subsumption_agent import Cell as Ssa
from .tools import ROBOT_SWORD, TAJ_MAJAL, TRIANGLE

AGENT = {'Fsm': Fsm, 'Ssa': Ssa}

FIGURE = {'robot': ROBOT_SWORD, 'taj': TAJ_MAJAL, 'triangle': TRIANGLE}

COORDINATES = set([])


def get_position(coord, total, depth=0):
    """Get position."""
    if depth > total:
        return
    left = (coord[0]-1, coord[1])
    down = (coord[0], coord[1]-1)
    COORDINATES.add(left)
    COORDINATES.add(down)
    get_position(down, total, depth+1)
    get_position(left, total, depth+1)


class KilobotsShapes(Model):
    """
    Represent the kilobot environment.

    :height: Height of the grid in which to place the Kilobots
    :width: Width of the grid in which to place the Kilobots
    :nkilobots: Number of kilobots
    :seed: Random generator's seed
    :architacture: any of Ssa or Fsm
    """

    def __init__(self, height=100, width=500, nkilobots=100, seed=None,
                 architect='Fsm', figure2make='triangle'):
        """
        Create a new playing area of (height, width) cells.
        """

        # Set up the grid and schedule.

        # Use SimultaneousActivation which simulates all the cells
        # computing their next state simultaneously.  This needs to
        # be done because each cell's next state depends on the current
        # state of all its neighbors -- before they've changed.
        self.schedule = SimultaneousActivation(self)

        # Use a simple grid, where edges wrap around.
        self.grid = Grid(height, width, torus=True)
        # select the architecture
        Cell = AGENT[architect]
        # compute the FIGURE
        fig_definition = FIGURE[figure2make]
        gradients_dict = defaultdict(lambda: (0, -1))
        robots_needed = 0
        for key, val in fig_definition.items():
            gradients_dict[key] = val
            robots_needed += val[1]-val[0]+1
        nkilobots = robots_needed - 2
        # create X kilobots and placethem next to eachother
        # for x in range(50):
        kilobot_id = 1
        seed_bots = ((28, 28, 1), (29, 28, 1), (28, 29, 2))
        self.triangle_sides = np.ceil((np.sqrt(8*nkilobots+1)-1)/2)
        print('********************', self.triangle_sides)
        self.nkilobots = (self.triangle_sides*(self.triangle_sides+1))/2
        self.square_sides = np.ceil(np.sqrt(self.nkilobots))
        get_position(seed_bots[0], self.triangle_sides)

        for (x_coord, y_coord) in sorted(COORDINATES, key=lambda x: (x[0]*2)+x[1], reverse=True):
            cell = Cell((x_coord, y_coord), kilobot_id,
                        self, gradients_dict, seed=seed)
            # if self.random.random() < 0.1:
            cell.state = "idle"
            self.grid.place_agent(cell, (x_coord, y_coord))
            self.schedule.add(cell)
            kilobot_id += 1
            if nkilobots <= kilobot_id:
                break

        for (x_coord, y_coord, gradient) in seed_bots:
            cell = Cell((x_coord, y_coord), 0, self, gradients_dict, seed=seed)
            cell.state = "seed"
            cell.gradient = gradient
            self.grid.place_agent(cell, (x_coord, y_coord))
            self.schedule.add(cell)

        self.running = True

    def step(self):
        """
        Have the scheduler advance each cell by one step
        """
        self.schedule.step()
