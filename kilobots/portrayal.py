states = {"seed": "kilobots/resources/kilobot_green.png",
          "idle": "kilobots/resources/kilobot.png",
          "positioned": "kilobots/resources/kilobot_blue.png",
          "edge": "kilobots/resources/kilobot_pink.png",
          "moving": "kilobots/resources/kilobot_pink.png",
          "lost": "kilobots/resources/kilobot_red.png"
          }



def portrayCell(cell):
    """
    This function is registered with the visualization server to be called
    each tick to indicate how to draw the cell in its current state.
    :param cell:  the cell in the simulation
    :return: the portrayal dictionary.
    """
    assert cell is not None
    return {
        "Shape": "circle",
        "r": 1,
        # "h": 1,
        "scale": 1,
        "Filled": "true",
        "Shape": states[cell.isAlive],
        "Layer": 0,
        "x": cell.whereami[0],
        "y": cell.whereami[1],
        # "Color": 'blue',
    }
